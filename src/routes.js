

const Apify = require('apify');
const { LABEL_DETAIL, OFFERS_LIST } = require('./variables');

const { utils: { log } } = Apify;


//
// PRODUCT SEARCH
//
exports.handleStart = async ({ request, page }, requestQueue) => {
    log.info(`HANDLE START URL - Processing ${request.url}`);

    // Scrape Data
    const data = await page.evaluate(() => {
        const productElements = Array.from(document.querySelectorAll('.s-main-slot > div'));
        const products = [];

        productElements.forEach((e) => {
            // Get component type (only interested in 's-search-result')
            const componentType = e.getAttribute('data-component-type');
            const isSearchResult = componentType && componentType === 's-search-result';
            if (!isSearchResult) {
                return;
            }

            // Filter sponsored results (can be earbuds etc., we're not interested)
            const sponsored = e.querySelector('.rush-component');
            const isSponsored = sponsored ? sponsored.getAttribute('data-component-type') === 'sp-sponsored-result' : false;
            if (isSponsored) {
                return;
            }

            // Final product => Get data
            const asin = e.getAttribute('data-asin');
            const title = asin && asin.length ? e.querySelector('h2 span').innerText : null;

            const isRelevant = asin && asin.length;
            if (!isRelevant) {
                return;
            }

            // Push relevant data
            products.push({ asin, title, url: `https://www.amazon.com/dp/${asin}` });
        });

        return products;
    });

    // Console log for debugging
    console.log('Scraped data for START URL:');
    console.log(data);

    // Push to requestQueue
    data.forEach((productData) => { requestQueue.addRequest({ url: productData.url, userData: { ...productData, label: LABEL_DETAIL } }); });
};


//
// PRODUCT DETAIL
//
exports.handleDetail = async ({ request, page }, requestQueue) => {
    log.info(`HANDLE DETAIL URL - Processing ${request.url}`);

    // Scrape Data
    const data = await page.evaluate(() => {
        let descriptionElement = document.getElementById('productDescription');
        let description = descriptionElement ? descriptionElement.innerText : null;

        if (!description) {
            descriptionElement = document.querySelector('#productDescription p');
            description = descriptionElement ? descriptionElement.innerText : null;
        }
        return { description };
    });

    // Console log for debugging
    console.log(`Scraped data for ${request.userData.asin}:`);
    console.log(data);

    // We've got additional data, let's request offers
    requestQueue.addRequest({
        url: `https://www.amazon.com/gp/offer-listing/${request.userData.asin}`,
        userData: { ...request.userData, ...data, label: OFFERS_LIST } });
};


//
// OFFERS LIST
//
exports.handleOffers = async ({ request, page }, keyword, dataset) => {
    log.info(`HANDLE OFFERS URL - Processing ${request.url}`);

    // Scrape Data
    const data = await page.evaluate(() => {
        const offerElements = Array.from(document.querySelectorAll('.olpOffer'));
        const offers = [];

        offerElements.forEach((e) => {
            // Get offer data
            const priceElement = e.querySelector('.olpOfferPrice');
            const price = priceElement ? priceElement.innerText : null;

            let sellerNameElement = e.querySelector('.olpSellerName a');
            let sellerName = sellerNameElement ? sellerNameElement.innerText : null;

            // Handle seller img instead of text (Amazon uses img, for example)
            if (!sellerName) {
                sellerNameElement = e.querySelector('.olpSellerName img');
                sellerName = sellerNameElement ? sellerNameElement.getAttribute('alt') : null;
            }

            // Not interested in partial information
            if (!price || !sellerName) {
                return;
            }

            // Push relevant data
            offers.push({ price, sellerName });
        });

        return offers;
    });

    // Console log for debugging
    console.log(`Scraped offers for ${request.userData.asin}:`);
    console.log(data);

    // Push to dataset
    data.forEach(async (offer) => {
        await dataset.pushData({
            title: request.userData.title,
            url: request.userData.url,
            description: request.userData.description,
            keyword,
            sellerName: offer.sellerName,
            price: offer.price,
            shippingPrice: 'todo',
        });
    });
};
