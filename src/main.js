const Apify = require('apify');
const { handleStart, handleDetail, handleOffers } = require('./routes');
const { LABEL_DETAIL, OFFERS_LIST } = require('./variables');

const { utils: { log } } = Apify;
const START_URL = 'https://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Daps&field-keywords=';

Apify.main(async () => {
    // Get keyword from INPUT.json
    const { keyword } = await Apify.getInput();

    // Open (clean) cloud dataset
    let dataset = await Apify.openDataset('default', { forceCloud: true });
    await dataset.drop();
    dataset = await Apify.openDataset('default', { forceCloud: true });

    // Open requestQueue and add the starting url as the first request
    const requestQueue = await Apify.openRequestQueue();
    await requestQueue.addRequest({ url: `${START_URL}${keyword}` });

    // Crawler
    const crawler = new Apify.PuppeteerCrawler({
        requestQueue,
        /* useSessionPool: true,
        persistCookiesPerSession: true, */
        launchPuppeteerOptions: {
            headless: true,
            useApifyProxy: true,
            useChrome: true,
            stealth: true,
            /* slowMo: 500 For debugging purposes */
        },
        handlePageFunction: async (context) => {
            const { url, userData: { label } } = context.request;
            log.info('Page opened.', { label, url });

            switch (label) {
                case OFFERS_LIST:
                    return handleOffers(context, keyword, dataset);
                case LABEL_DETAIL:
                    return handleDetail(context, requestQueue);
                default:
                    return handleStart(context, requestQueue);
            }
        },
    });


    // Run crawler
    log.info('Starting the crawl.');
    await crawler.run();
    log.info('Crawl finished.');

    // Get public url to cloud dataset
    const datasetPublicUrl = `https://api.apify.com/v2/datasets/${dataset.datasetId}/items`;
    log.info(`Public url generated: ${datasetPublicUrl}`);

    // Send email
    /* const to = 'jan.hamernik@bootiq.io';
    const subject = 'Bootiq';
    let text = 'Name: Jan Hamernik (BOOTIQ)';
    text += '\n\nThis is for the Apify SDK exercise';
    text += `\n\nPublic link to dataset: ${datasetPublicUrl}`;

    log.info(`Sending email to ${to}.`);
    await Apify.call('apify/send-mail', { to, subject, text });
    log.info('Email sent'); */

    // Send email (OFFICIAL)
    /* const to2 = 'lukas@apify.com';
    log.info(`Sending email to ${to2}.`);
    await Apify.call('apify/send-mail', { to: to2, subject, text });
    log.info('Email sent'); */
});
